\select@language {czech}
\contentsline {chapter}{\IeC {\'U}vod}{12}{section*.5}
\contentsline {chapter}{\numberline {1}Problematika SLAM algoritm\IeC {\r u}}{13}{chapter.6}
\contentsline {section}{\numberline {1.1}Simultanious Localization and Mapping (SLAM)}{13}{section.7}
\contentsline {subsection}{\numberline {1.1.1}Graph Based SLAM (Pose Graph)}{14}{subsection.10}
\contentsline {subsection}{\numberline {1.1.2}Dense SALM}{16}{subsection.20}
\contentsline {subsection}{\numberline {1.1.3}Bundle Adjustment}{16}{subsection.21}
\contentsline {section}{\numberline {1.2}Reprezentace prostoru}{18}{section.25}
\contentsline {subsection}{\numberline {1.2.1}Point Cloud}{18}{subsection.26}
\contentsline {subsection}{\numberline {1.2.2}Octree}{19}{subsection.28}
\contentsline {section}{\numberline {1.3}Pojmy a metody p\IeC {\v r}i SLAM}{19}{section.30}
\contentsline {subsection}{\numberline {1.3.1}Keyframe}{20}{subsection.31}
\contentsline {subsection}{\numberline {1.3.2}Iterative Closest Point (ICP)}{20}{subsection.33}
\contentsline {subsection}{\numberline {1.3.3}Loop Close}{21}{subsection.34}
\contentsline {subsection}{\numberline {1.3.4}RANSAC}{21}{subsection.36}
\contentsline {subsection}{\numberline {1.3.5}Signed Distance Function (SDF)}{24}{subsection.42}
\contentsline {chapter}{\numberline {2}Implementace 3D SALM algoritm\IeC {\r u}}{26}{chapter.48}
\contentsline {section}{\numberline {2.1}Kinect Fusion}{26}{section.49}
\contentsline {section}{\numberline {2.2}Kintinuous}{27}{section.51}
\contentsline {section}{\numberline {2.3}Elastic Fusion}{28}{section.53}
\contentsline {section}{\numberline {2.4}Dense Visual SLAM}{29}{section.55}
\contentsline {section}{\numberline {2.5}RGBDSLAM - 6DOF SLAM}{31}{section.57}
\contentsline {section}{\numberline {2.6}Komparace algoritm\IeC {\r u}}{32}{section.59}
\contentsline {chapter}{\numberline {3}RGB-D kamery}{34}{chapter.60}
\contentsline {section}{\numberline {3.1}Aktivn\IeC {\'\i } triangulace}{34}{section.61}
\contentsline {section}{\numberline {3.2}Pasivn\IeC {\'\i } triangulace}{35}{section.64}
\contentsline {section}{\numberline {3.3}Princip Time of Flight}{37}{section.71}
\contentsline {section}{\numberline {3.4}Distance-Varying Illumination and Imaging Techniques for Depth Mapping}{40}{section.86}
\contentsline {section}{\numberline {3.5}Princip strukturovan\IeC {\'e}ho sv\IeC {\v e}tla}{42}{section.89}
\contentsline {section}{\numberline {3.6}P\IeC {\v r}ehled dostupn\IeC {\'y}ch RGB-D kamer}{43}{section.91}
\contentsline {subsection}{\numberline {3.6.1}Kinect v1 (Xbox 360)}{43}{subsection.92}
\contentsline {subsection}{\numberline {3.6.2}Kinecv v2 (Xbox One)}{43}{subsection.93}
\contentsline {subsection}{\numberline {3.6.3}Swiss Ranger SR4000}{44}{subsection.94}
\contentsline {subsection}{\numberline {3.6.4}Asus Xtion Pro Live}{44}{subsection.95}
\contentsline {subsection}{\numberline {3.6.5}SoftKinetic DS525}{44}{subsection.96}
\contentsline {subsection}{\numberline {3.6.6}Orbbec Astra Pro}{45}{subsection.97}
\contentsline {subsection}{\numberline {3.6.7}Orbbec Persee}{45}{subsection.98}
\contentsline {subsection}{\numberline {3.6.8}ZED Stereo Camera}{45}{subsection.99}
\contentsline {section}{\numberline {3.7}V\IeC {\'y}b\IeC {\v e}r sn\IeC {\'\i }ma\IeC {\v c}e}{46}{section.100}
\contentsline {section}{\numberline {3.8}Geometrick\IeC {\'a} kalibrace Kinectu v2}{46}{section.101}
\contentsline {subsection}{\numberline {3.8.1}Model d\IeC {\'\i }rkov\IeC {\'e} komory}{46}{subsection.102}
\contentsline {subsection}{\numberline {3.8.2}Geometrie jednoduch\IeC {\'e}ho prom\IeC {\'\i }t\IeC {\'a}n\IeC {\'\i } \(R^3\)\IeC {\textrightarrow } \(R^2\)}{47}{subsection.104}
\contentsline {subsection}{\numberline {3.8.3}Zkreslen\IeC {\'\i } sn\IeC {\'\i }ma\IeC {\v c}e}{49}{subsection.113}
\contentsline {subsection}{\numberline {3.8.4}Proces kalibrace}{50}{subsection.118}
\contentsline {subsection}{\numberline {3.8.5}Parametry hloubkov\IeC {\'e} kamery}{52}{subsection.123}
\contentsline {chapter}{\numberline {4}Tvorba testovac\IeC {\'\i }ho datasetu}{55}{chapter.126}
\contentsline {section}{\numberline {4.1}Hardwarov\IeC {\'e} vybaven\IeC {\'\i }}{55}{section.127}
\contentsline {section}{\numberline {4.2}Softwarov\IeC {\'e} vybaven\IeC {\'\i }}{55}{section.128}
\contentsline {subsection}{\numberline {4.2.1}Ubuntu 14.04}{55}{subsection.129}
\contentsline {subsection}{\numberline {4.2.2}OpenNI2 framework}{56}{subsection.130}
\contentsline {subsection}{\numberline {4.2.3}Ovlada\IeC {\v c} libfreenect2}{56}{subsection.131}
\contentsline {subsection}{\numberline {4.2.4}Qt Framework}{57}{subsection.132}
\contentsline {subsection}{\numberline {4.2.5}.ONI soubor}{57}{subsection.134}
\contentsline {section}{\numberline {4.3}Po\IeC {\v r}\IeC {\'\i }zen\IeC {\'\i } testovac\IeC {\'\i }ch dat}{58}{section.135}
\contentsline {subsection}{\numberline {4.3.1}Data po\IeC {\v r}\IeC {\'\i }zen\IeC {\'a} Kinectem v2}{58}{subsection.136}
\contentsline {subsection}{\numberline {4.3.2}Data po\IeC {\v r}\IeC {\'\i }zen\IeC {\'a} ToF Faro X330}{59}{subsection.138}
\contentsline {subsection}{\numberline {4.3.3}Uk\IeC {\'a}zka Elastic Fusion}{59}{subsection.140}
\contentsline {chapter}{\numberline {5}N\IeC {\'a}vrh \IeC {\'u}pravy 3D SLAMu}{62}{chapter.143}
\contentsline {section}{\numberline {5.1}Roz\IeC {\v s}\IeC {\'\i }\IeC {\v r}en\IeC {\'\i } sou\IeC {\v c}asn\IeC {\'y}ch SLAM algoritm\IeC {\r u}}{62}{section.144}
\contentsline {section}{\numberline {5.2}Diskr\IeC {\'e}tn\IeC {\'\i } Kalman\IeC {\r u}v filtr}{62}{section.146}
\contentsline {section}{\numberline {5.3}Ro\IeC {\v s}\IeC {\'\i }\IeC {\v r}en\IeC {\'y} Klaman\IeC {\r u}v filtr}{65}{section.158}
\contentsline {section}{\numberline {5.4}F\IeC {\'u}ze kolov\IeC {\'e} a vizu\IeC {\'a}ln\IeC {\'\i } odometrie}{65}{section.161}
\contentsline {section}{\numberline {5.5}Kolov\IeC {\'a} odometrie (Dead Reckoning)}{66}{section.162}
\contentsline {section}{\numberline {5.6}Metoda m\IeC {\v e}\IeC {\v r}en\IeC {\'\i } p\IeC {\v r}esnosti vytvo\IeC {\v r}en\IeC {\'y}ch model\IeC {\r u}}{67}{section.170}
\contentsline {chapter}{\numberline {6}Z\IeC {\'a}v\IeC {\v e}r}{69}{chapter.172}
\contentsline {chapter}{Literatura}{70}{section*.174}
\contentsline {chapter}{Seznam symbol\r {u}, veli\v {c}in a zkratek}{71}{section*.176}
\contentsline {chapter}{Seznam p\v {r}\'{\i }loh}{72}{section*.178}
\contentsline {chapter}{\numberline {A}N\IeC {\v e}kter\IeC {\'e} p\IeC {\v r}\IeC {\'\i }kazy bal\IeC {\'\i }\IeC {\v c}ku \texttt {thesis} }{73}{chapter.179}
\contentsline {section}{\numberline {A.1}P\IeC {\v r}\IeC {\'\i }kazy pro sazbu veli\IeC {\v c}in a jednotek }{73}{section.180}
\contentsline {section}{\numberline {A.2}P\IeC {\v r}\IeC {\'\i }kazy pro sazbu symbol\IeC {\r u} }{73}{section.182}
\contentsline {chapter}{\numberline {B}Druh\IeC {\'a} p\IeC {\v r}\IeC {\'\i }loha }{74}{chapter.184}
\contentsline {chapter}{\numberline {C}Obsah p\IeC {\v r}ilo\IeC {\v z}en\IeC {\'e}ho CD }{75}{chapter.186}
