
\chapter{Problematika SLAM algoritmů}


V této kapitole je popsán matematický aparát a základní principy, na jejichž bázi se provádí sebelokalizace a mapování okolí robota.

\section{Simultanious Localization and Mapping (SLAM)}

Problematika SLAMu je postavena na paralelní tvorbě mapy, modelu okolí, a hledání vlastní pozice v tomto modelu. Už na první pohled je patrné, že tyto dva problémy jsou úzce provázaným protože bez mapy není možné určovat polohu, a bez znalosti polohy není možné vytvořit mapu.

\begin{figure}[h!]
	\begin{center}
    		\includegraphics[scale=0.4]{./obrazky/1a.png}
    		\caption{Schématické znázornění SLAM problematiky}
		\label{imgSLAM}
	\end{center}
\end{figure}



Uvažujme tedy, že na začátku se robot nachází v zcela novém prostředí na pozici \(x_0\). Následně provede akční zásah, který jej přesune do pozice \(x_1\), a měření \(z_1\), kterým získá upřesňující informace o své poloze. Úplné řešení SLAM problematiky pak spočívá ve stanovení nejpravděpodobnější polohy na bázi znalosti mapy, měření a akčního zásahu.

\begin{equation}
p(x_{1:N},m | z_{1:N}, u_{1:N}, x_0)
\label{eq1}
\end{equation}

V problematice tvorby map se setkáváme se dvěma nejčastějšími přístupy. Prvním z nich je tzv. „Landmark mapping“, tedy mapa, která je definována vysoce kontrastními předpřipravenými značkami v mapovaném prostředí. Představme si například QR kódy na stěnách. Robot poté na základě svých měření provádí výpočty vzdáleností k jednotlivým landmarkům a na základě toho určuje svou polohu.

Druhou variantou je tzv. „Dense map“. Jedná se o mapu tvořenou point cloudem, tedy velkým počtem bodů s definovanou polohou v prostoru, který je výstupem například z LIDARu. V takto vzniklé mapě pak robot provádí určení své polohy korelací svého aktuálního měření  s mapou a minimalizací například kvadratické chyby vzdáleností mezi prostorovými body.

\subsection{Graph Based SLAM (Pose Graph)}

Myšlenka graph based slamu je postavená na tvorbě grafu, ve kterém jsou zaneseny jednotlivé polohy robota a tzv. landmark jako uzly grafu, a mezi těmito uzly jsou vytvořeny vazby, které vyjadřují jejich vzájemné prostorové vztahy.

\begin{figure}[h!]
	\begin{center}
    		\includegraphics[scale=0.5]{./obrazky/2a.png}
    		\caption{Schéma Graph SLAMu, pozice robota,
		 landmarky a vazby meze nimi tvoří celistvý graf reprezentující okolí.}	
		 \label{imgGraphSLAM}
	\end{center}
\end{figure}

Představme si robota v nulové výchozí pozici . Zároveň tuto polohu zaneseme do grafu jako první uzel. Poté robot provede přesun z výchozího stavu  na novou pozici. Pokud tato nova pozice splňuje kriteriální podmínku, například, že je dostatečně vzdálený od předchozího uzlu, pak je tato nova poloha zanesená do grafu, jako nově vzniklý uzel a mezi takto vzniklými prvky grafu se vytvoří vazba.

Takto vzniklý graf je ale velmi nepřesný, protože vlivem nepřesností měření velmi rychle vzniká v určení polohy velká kumulativní chyba. Tu je možné korigovat měřením okolí. A právě nyní zavedeme do této metody již dříve zmíněné landmarky. Může se jednat o překážky, významné body v prostoru nebo rohy objektů. Robot zjistí jejich pozici svým senzorickým vybavením relativně vůči své aktuální pozici. Poté vypočte svou relativní pozici vůči uzlům grafu a z těchto dvou prostorových transformací vypočte absolutní polohu landmarku v rámci grafu a zavede jej, jako nový uzel. Touto cestou vzniká síť která reprezentuje již prozkoumaný prostor a zaznamenává v něm klíčové orientační body. Častěji se však v praxi referenčními uzly grafu stávají tzv. keyframy, které jsou popsány dále v této kapitole.

\begin{equation}
p(z_t | x_t, m_t)
\label{eq2}
\end{equation}

Rovnice vyjadřuje, že výsledek měření \(z_t\) je funkcí polohy robota \(x_t\) v rámci mapy \(m_t\).

\begin{equation}
p(x_{1:T} | z_{1:T}, u_{1:T})
\label{eq3}
\end{equation}

Následující rovnice říká, že pozice robota \(x\) v rámci modelu \(m\) je závislá na měření \(z\), akčním zásahu \(u\).

Mějme tedy vektor \(x = [x_0, x_1, ..., n_n]\), který reprezentuje množinu všech referenčních pozic (uzlů) robota v grafu. Dále zaveďme \(z_{ij}\) a , \(\omega_{ij}\)což jsou střední hodnota a informativní matice měření mezi body \(i\) a \(j\). Dále \(\hat{z}_{ij}(x_i, x_j)\) je predikcí vztahu mezi dvěma uzly. Pak věrohodnost měření \(z_{ij}\) stanovíme jako

\begin{equation}
l_{ij} = [z_{ij} - \hat{z}_{ij}(x_i, x_j)]^T \Omega_{ij} [z_{ij} - \hat{z}_{ij}(x_i, x_j)]
\label{eq4}
\end{equation}


\begin{figure}[h!]
	\begin{center}
    		\includegraphics[scale=0.4]{./obrazky/3a.png}
    		\caption{Chyba vzniklá mezi změřenou a skutečnou polohou}	
		 \label{imgSLAMError}
	\end{center}
\end{figure}

Do této rovnice můžeme zanést substituci reprezentující chybu měření

\begin{equation}
e_{ij}(x_i, x_j) = z_{ij} - \hat{z}_{ij}(x_i, x_j)
\label{eq5}
\end{equation}

Poté se výše uvedená rovnice pro věrohodnost měření redukuje na

\begin{equation}
l_{ij} = e_{ij}^T \Omega_{ij} e_{ij}
\label{eq6}
\end{equation}

a nyní můžeme uvažovat nejpravděpodobnější polohu robota v rámci grafu, jako konfiguraci grafových uzlů a hran s minimalizovaným součtu všech chyb mezi jednotlivými uzly.

\begin{equation}
E(x) = \sum_{i,j} l_{ij}
\label{eq7}
\end{equation}

\begin{equation}
x' = argmin E(x)
\label{eq8}
\end{equation}

\subsection{Dense SALM}

Jedná se o dnes nejfrekventovanější způsob reprezentace modelu okolí, kdy mapa je tvořena tzv. point cloudem, tedy množinou prostorových bodů, které reprezentují místo, ve kterém byl zaznamenán odraz od překážky. 
Sebelokalizace v rámci této mapy pak obvykle probíhá tak, že robot vytvoří point cloud svého aktuálně pozorovaného okolí, a to se posléze snaží lokalizovat v rámci mapy tak, že iterativně stanovuje transformaci a počítá chybu překrytí obou množin bodů.


\subsection{Bundle Adjustment}

Bundle adjustment je metoda rekonstrukce trojrozměrného objektu z obrazových dat, která byla pořízena z různých míst a různých úhlů pomocí jednoho snímače. Nejčastěji je tedy používaná k rekonstrukci modelů statických trojrozměrných objektů.

\begin{figure}[h!]
	\begin{center}
    		\includegraphics[scale=0.4]{./obrazky/4a.png}
    		\caption{Grafické znázornění chyby promítání trojrozměrného bodu na rovinu snímače}	
		 \label{imgBundleAdjustment}
	\end{center}
\end{figure}

Představme si situaci, kdy disponujeme  snímky trojrozměrného objektu, ale neznáme kalibrační parametry snímače (matice intrinzických parametrů) ani jeho přesnou polohu a natočení (matice extrinzických parametry) při pořizování jednotlivých obrázků. Úkolem této metody je najít právě výše zmíněné vnitřní a vnější parametry snímače a následně provést rekonstrukci jednotlivých nasnímaných bodů do trojrozměrného prostoru tak, aby byla minimalizována deklarovaná chybová funkce.

\begin{equation}
Err(\textbf{v}, \textbf{a}, \textbf{b}, \textbf{x}) = \sum_{i=1}^n \sum_{j=1}^m v_{ij} d(\textbf{Q}(a_j, b_i), x_{ij})^2
\label{eq9}
\end{equation}

\begin{equation}
\min\limits_{a,b} ( Err(\textbf{v}, \textbf{a}, \textbf{b}, \textbf{x}) )
\label{eq10}
\end{equation}

kde \(\textbf{Q}(a_j, b_i)\) vyjadřuje polohu \(i\)-tého bodu na \(j\)-té promítací rovině, tedy předpokládaná pozice bodu \(i\) na snímku \(j\). \(a_j\) je vektorem kalibračních parametrů snímače a \(b_i\) je pozice bodu v rekonstruovaném trojrozměrném prostoru. Následně funkce \(d(\textbf{Q}, x)\) vypočte euklidianskou vzdálenost mezi předpokládanou polohou \(\textbf{Q}\) a skutečnou nasnímanou polohou bodu \(x_{ij}\). \(v_{ij}\) vyjadřuje booleovskou hodnotu, zda se \(i\)-tý bod nachází v zorném poli \(j\)-tého snímače.


Na rozdíl od real-time SLAM algoritmů, bundle adjustment je metodou postrocesingovou, tedy je prováděna nad již dříve pořízenými offline daty. Některé SLAM implementace však používají tuto metodu pro průběžné zpřesnění svých výsledků.


Obecně je bundle adjustment metodou výpočetně náročnější, než SLAM, ale poskytuje lepší výsledky.   


Příklad aplikace bundle adjustment na data z RGB-D kamery je popsán v \cite{cite07}.


\section{Reprezentace prostoru}

Tato podkapitole uvádí několik způsobů, jak je možné reprezentovat a uložit data, která představují okolí robota provádějícího SLAM. 

\subsection{Point Cloud}


V informatice tento termín vyjadřuje množinu bodů umístěných do určitého souřadného systému. V rámci robotiky a problematiky SLAMu pak můžeme konkrétně hovořit o množině bodů umístěných v trojrozměrné kartézské soustavě (souřadnice X, Y, Z). 


Tato forma dat vzniká například měříme-li prostor LIDARem. Ten provádí měření vždy s pevným krokem azimutu a vertikálního sklonu. Výsledkem je sada N bodů umístěných do prostoru, které reprezentují místo ve kterém se laserový paprsek odrazil od překážky.

\begin{figure}[h!]
	\begin{center}
    		\includegraphics[scale=0.5]{./obrazky/5.png}
    		\caption{Point Cloud reprezentující chodbu}	
		 \label{imgPointCloud}
	\end{center}
\end{figure}

V našem případě point cloud vzniká interpretací pixelů RGB-D kamery promítnutých do prostoru. Sada takto umístěných bodů tvoří výsledný model ve kterém se robot sebelokalizuje a dále mapu rozšiřuje.

Pro práci s touto formou dat existuje několik open source knihoven. Nejčastěji je používaná například Point Cloud Library (PCL) \cite{cite03}.

\subsection{Octree}

Jedná se o hierarchickou reprezentaci obsazenosti prostoru. Prostor je na začátku definován jednou krychlí, která o sobě deklaruje, zda je obsazená úplně, částečně či nikoliv. Pokud je obsazena jen částečně provedeme její pomyslné rozdělení na 8 subkrychlí vždy s poloviční délkou hrany a následně pro každou takto vzniklou subkrychli opět klademe otázku do jaké míry je obsazená. Takto můžeme rekurzivně postupovat na úroveň libovolně malého objemu.

\begin{figure}[h!]
	\begin{center}
    		\includegraphics[scale=0.5]{./obrazky/6.png}
    		\caption{Hierarchická reprezentace octree struktury \cite{cite04}}	
		 \label{imgOctree}
	\end{center}
\end{figure}


V praxi \cite{cite05} tento formát dat může být uložen například jako lineární pole, ve kterém vždy 2 bity vyjadřují, zda je krychle zaplněná úplně, částečně či nikoliv. Pokud tyto dva bity říkají, že je oblast prázdná, nebo úplně zaplněná, průzkum dané oblasti ukončujeme a v rekurzivním algoritmu se vracíme o úroveň výše. Tento poslední bod v hierarchii nazýváme list. Pokud dvoj bit udává částečné obsazení, pomyslně dělíme prostor na 8 subkrychlí a čteme další 2 bity v poli a přiřazujeme je první z osmi subkrychlí. V okamžiku kdy je vyřešena dekomprese v první subkrychli čteme následující dva bity a použijeme je pro popis druhé subkrychle, atd. 


\section{Pojmy a metody při SLAM}

V této podkapitole je popsáno několik termínů a metod, které jsou dále v práci používány při popisu jednotlivých řešení.

\subsection{Keyframe}

Jako keyframe je obvykle v problematice SLAM myšlen snímek, který splnil kriteriální podmínku pro vznik nového uzlu v grafu a je tedy použit jako nový prvek do sady bodů, které se používají jako reference pro určování polohy a expanze grafu.

Již zmíněná kriteriální podmínka je heuristicky zvolená mez, po jejímž překročení je do grafu přidán nový uzel. Podmínka může být určená mezním vzdálením se od již existujících uzlů, snížením přesnosti sebelokalizace, a tím pádem nutností zavést nový referenční bod, atd.

\begin{figure}[h!]
	\begin{center}
    		\includegraphics[scale=0.5]{./obrazky/7.png}
    		\caption{Keyframy jsou obvykle graficky interpretovány jako objekty reprezentující frustum kamery na jeji trajektorii v čase \(t\) \cite{cite34}}	
		 \label{imgKeyframe}
	\end{center}
\end{figure}


\subsection{Iterative Closest Point (ICP)} 

Tato metoda \cite{cite06} slouží k nalezení transformační matice se šesti stupni volnosti (tři pro rotaci a tři pro translaci) mezi dvěma point cloudy. Jedna ze soustav (mapa) je statická a druhá (data z měření) jsou iterativně transformována tak, aby bylo dosaženo co nejmenší kvadratické odchylky mezi všemi body.

V praxi se v robotice používá pro nalezení pozice robota v rámci modelu, přičemž robot zná své okolí. 

V každém kroku algoritmus provádí nalezení nejbližšího bodu z množiny A k bodu v množině B, vypočte sumu kvadrátů všech těchto vzdáleností, na základě derivace chyby provede úpravu transformace jedné ze soustav a tyto kroky opakuje, dokud není splněná ukončovací podmínka.

Problémem algoritmu je, že může uvíznout v lokálním minimu chybové funkce. Proto není vhodný na globální prohledávání prostoru, ale pouze na drobné doladění polohy, u které apriorně známe přibližnou polohu robota, respektive snímače, který provedl sken okolí.

\subsection {Loop Close}


Při tvorbě grafu, jak již bylo zmíněno, dochází neustále ke kumulaci chyby měření, a to jak při určování polohy robota podle vizuální, nebo mechanické odometrie. To má za následek, že pokud robot urazí určitou trajektorii a poté se vrátí do výchozí pozice, takto vznikly graf nebude konzistentní a uzly vyjadřující počáteční a současnou polohu nebudou nabývat shodných souřadnic. Tím samozřejmě vzniká nejistota při dalším určování polohy, protože jeden a tentýž bod je v modelu okolí reprezentován dvakrát. Pokud ale stanovíme kritérium, po jehož splnění prohlásíme dva rozdílná místa v rámci mapy za shodné, pak můžeme provést deformaci mapy tak, aby tyto dva místa byla sjednocena na pozici dříve vytvořeného uzlu, protože můžeme předpokládat, že při jeho vzniku byla naakumulována menší chyba určení pozice.

To sebou samozřejmě nese problém, že přesunutím jednoho uzlu narušíme vazby mezi zbývajícími uzly na právě uzavřené smyčce. Korekce pozic těchto zbývajících uzlů na celé trajektorii smyčky se nazývá relaxace, někdy také optimalizace grafu.

Existuje mnoho metod jak je tato relaxace prováděná. Jako příklad můžeme například zmínit metody \cite{cite08}, \cite{cite32}, kdy jednotlivé uzly mezi sebou nabývají vazeb s Gaussovou distribucí. Pokud potom přesuneme jeden uzel, dojde k řetězovému posunu distribuce pravděpodobnosti výskytu jednotlivých navazujících uzlů na celé trase uzavřené smyčky.
 
\begin{figure}[h!]
	\begin{center}
    		\includegraphics[scale=0.5]{./obrazky/8.png}
    		\caption{Přiklad nalezení uzavřené smyčky a relaxace grafu \cite{cite09}}	
		 \label{imgLoopClose}
	\end{center}
\end{figure}

\subsection{RANSAC}

RANSAC \cite{cite10} je iterativní metoda pro stanovení neznámého matematického modelu v souboru dat, který obsahuje tzv. „outlayers“, tedy prvky, které leží mimo hledaný model (vznikají například šumem měření) a tzv. „inlayers“, tedy elementy splňující kritérium hledaného modelu.

Vstupem algoritmu je sada naměřených dat, ve které chceme nalézt závislost.

Nejprve algoritmus náhodně vybere \(N\) prvků, kde \(N\) je minimální počet vstupních dat pro vyřešení všech stupňů volnosti modelu, a posléze se provede výpočet modelu.

Následně je tento model testován na celé množině vstupních dat. Ty projdou posouzením, zda-li odpovídají stanovenému modelu s tolerancí \(\mu\), a pokud ano jsou zařazeny do skupiny inlayers nebo případně outlayers, pokud je tomu naopak. Je-li pak procentuální zastoupení inlayers v předloženém datasetu větší než \(K\), kde \(0 < K \leq 1\), je možné takto nalezený model prohlásit za věrohodný a přijmout jej.

Pokud proběhla již \(L\)-tá iterace a model stále není určen, algoritmus se ukončí s tím, že se model nepovedlo nalézt.

Pokud nalezený model nesplňuje dostatečnou věrohodnost, je celý algoritmus iterován znova. 

\begin{figure}[h!]
	\begin{center}
    		\includegraphics[scale=0.45]{./obrazky/9.png}
    		\caption{Model nalezený RUNSUC metodou, výsledný model je netečný na body s velkou chybou \cite{cite39}}	
		 \label{imgRANSAC}
	\end{center}
\end{figure}

\begin{figure}[h!]
	\begin{center}
    		\includegraphics[scale=0.35]{./obrazky/10.png}
    		\caption{Ukázka inlayers (zeleně) a outlayers (červeně) při hledání transformace 
mezi dvěma obrazy pomocí RUNSUC metody}	
		 \label{imgRANSAC2d}
	\end{center}
\end{figure}

Tato metoda však nezaručuje, nalezení vhodného modelu se stoprocentní jistotou. Vždy existuje pravděpodobnost, se kterou každá z náhodně vybraných množin obsahovala alespoň jeden outlayer a tedy stanovený model není pravdivý.

Při inicializaci programu ale můžeme stanovit hodnotu \(L\), tedy kolikrát se má RUNSUC iterovat a hledat nejvhodnější model, než provede jeho posouzení, tak aby pravděpodobnost nalezení modelu byla například nejméně 0.99.

\begin{equation}
1 - p = (1 - u^m )^l
\label{eq11}
\end{equation}

po úpravě

\begin{equation}
L = \frac{log(1 - p)}{log( 1 - (1 - v)^m )}
\label{eq12}
\end{equation}
 
,kde \(L\) je počet iterací, \(p\) je pravděpodobnost nalezení pravého modelu \(u\) je pravděpodobnost, že náhodně vybraný bod z datasetu je inlayer, \(v\) je pravděpodobnost, že náhodně vybraný bod je outlayer.

\begin{equation}
u = 1 - v
\label{eq13}
\end{equation}


\subsection{Signed Distance Function (SDF)}

V oblasti počítačové grafiky se obvykle i složité objekty reprezentují skládáním mnoha trojúhelníků a vzniká tak aproximace požadovaného tvaru. Tato technika ovšem selhává v okamžiku, kdy chceme například rendrovat kouli, či jiné oblé prostorové útvary. Jednak si reprezentace těchto tvarů vyžádá velký počet polygonů, což sebou nese zvýšení výpočetní náročnosti při vykreslování, a navíc je tvar je tohoto objektu jen velmi nepřesnou aproximací skutečnosti.

V těchto případech je vhodné takovýto objekt reprezentovat například funkcí, které bude vracet záporné hodnoty, pokud se sledovaný bod nachází uvnitř objektu, nulu na jejím povrchu a kladné hodnoty pro souřadnice nacházející se mimo objekt.

Představme si jednoduchou funkci pro reprezentaci koule

\begin{equation}
A = f_{ball}(\textbf{p}, \textbf{c}, r) =
\begin{cases} 
      A = -1, & pro d(\textbf{p}, \textbf{c}) < r \\
      A = 0, & pro d(\textbf{p}, \textbf{c}) = r \\
      A = 1, & pro d(\textbf{p}, \textbf{c}) > r 
   \end{cases}
\label{eq13}
\end{equation}

,kde \(\textbf{p}\) je zkoumaný bod v prostoru, \(\textbf{c}\) je střed koule, \(r\) je její rádius koule a \(d\) je euklidovská vzdálenost bodu \(\textbf{p}\) a \(\textbf{c}\). Dále pak

\begin{equation}
d = \sqrt{\sum_{i=1}^{N}(p_i - c_i)^2}
\label{eq14}
\end{equation}

,kde  je počet dimenzí prostoru, ve kterých se nachází bod \(\textbf{p}\) a \(\textbf{c}\).

Složitější prostorové útvary pak můžeme reprezentovat skládáním jednodušších funkcí. Například průnik dvou tvarů definujeme jako

\begin{equation}
max( f_{ball1}(\mathbf{p}, \mathbf{c_1}, r_1), f_{ball2}(\textbf{p}, \mathbf{c}_2, r_2))
\label{eq15}
\end{equation}

a spojení dvou objektů provedeme

\begin{equation}
min( f_{ball1}(\mathbf{p}, \mathbf{c_1}, r_1), f_{ball2}(\textbf{p}, \mathbf{c}_2, r_2))
\label{eq16}
\end{equation}

\begin{figure}[h!]
	\begin{center}
    		\includegraphics[scale=0.35]{./obrazky/11a.png}
    		\caption{Výsledek skládání dvou SDF koulí. Vlevo minimum (sjednocení), vpravo maximu (průnik)}
		 \label{imgBalls}
	\end{center}
\end{figure}


Pokud tuto techniku aplikujeme na reprezentaci snímaného prostoru v problematice SLAM, dosáhneme jednak snížení nároků na paměť, protože tisíce prostorových bodů zastoupíme několika rovnicemi, zejména pak ale jsme schopni nasnímaný prostor generalizovat a potlačit v něm šum, protože například členitý zašuměný povrch objektu zastoupíme vyhlazeným geometrickým primitivem.




