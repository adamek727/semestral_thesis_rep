#include "kinectdatalogger.h"

KinectDataLogger::KinectDataLogger(){

    runLoop = true;
    iteratorCounter = 0;
    startupTime = new EpochTimestamp;
    if(startupTime == nullptr){
        cout << "EpochTimestamp not initalized" << endl;
    }
    startupTime->setupCurrentTime();

    cout << "Kinect Data Logger initialized in: " << startupTime->getTimestampString() << endl;

    device = new KinectDeviceReader;
    if(device == nullptr){
        cout << "KinectDeviceReader not initalized" << endl;
    }

    gui = new GuiPrezenter;
    if(gui == nullptr){
        cout << "GUI Prezenter not initalized" << endl;
    }

    storage = new StorageInterface;
    if(storage == nullptr){
        cout << "Storage Interface not initalized" << endl;
    }
}



void KinectDataLogger::mainLoop(){

    storage->makeFolderWithName(startupTime->getTimestampString());

    while(runLoop){

        iteratorCounter++;
        cout << "interated loop no.: " << iteratorCounter << endl;

        Mat colorImage = device->getColorFrame();
        //gui->drawImageWithName(colorImage,"color img");

        Mat depthImage = device->getDepthFrame();
        //gui->drawImageWithName(depthImage,"depth img");

        EpochTimestamp currentTimeStamp;
        currentTimeStamp.setupCurrentTime();
        string timeString = currentTimeStamp.getMilisecondTimestampString();

        char depthSavePath[255];
        sprintf(depthSavePath, "%s/depth_%s.bmp", startupTime->getTimestampString().c_str(), timeString.c_str());
        storage->saveImageWithPath(depthImage, string(depthSavePath));

        char colorSavePath[255];
        sprintf(colorSavePath, "%s/color_%s.bmp", startupTime->getTimestampString().c_str(), timeString.c_str());
        storage->saveImageWithPath(colorImage, string(colorSavePath));
    }
}

void KinectDataLogger::stopMainLoop(){

    runLoop = false;
}
