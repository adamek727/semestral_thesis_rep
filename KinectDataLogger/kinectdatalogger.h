#ifndef KINECTDATALOGGER_H
#define KINECTDATALOGGER_H

#include <stdio.h>
#include <chrono>
#include <string>

#include "epochtimestamp.h"
#include "kinectdevicereader.h"
#include "guiprezenter.h"
#include "storageinterface.h"

class KinectDataLogger
{
public:

    KinectDataLogger();

    void mainLoop();

    void stopMainLoop();

private:

    bool runLoop = false;

    long unsigned int iteratorCounter;

    EpochTimestamp *startupTime;

    KinectDeviceReader *device;

    GuiPrezenter *gui;

    StorageInterface *storage;
};

#endif // KINECTDATALOGGER_H
