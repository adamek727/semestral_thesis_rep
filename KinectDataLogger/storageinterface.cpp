#include "storageinterface.h"

StorageInterface::StorageInterface()
{
}

void StorageInterface::makeFolderWithName(string foldeName){

    char command[255], path[255];
    sprintf(command, "mkdir %s", foldeName.c_str());
    sprintf(path, "%s", foldeName.c_str());


    cout << "creating folder: " << command << endl;
    system(command);
    folderCreated = doesDirectoryExist(path);
    if(folderCreated){
        cout << "Directory created with name " << foldeName << endl;
    }else{
        cout << "Unable to create directory with name" << foldeName << endl;
    }
}


void StorageInterface::saveImageWithPath(Mat image, string imagePath){

    cv::imwrite(imagePath,image);
}



bool StorageInterface::doesDirectoryExist(string path){

    return true;

    const char *charPath = path.c_str();
    DIR* dir = opendir(charPath);
    if (dir){
        closedir(dir);
        return true;
    }
    else if (ENOENT == errno){
        /* Directory does not exist. */
        return false;
    }
    else{
        /* opendir() failed for some other reason. */
        return false;
    }
}
