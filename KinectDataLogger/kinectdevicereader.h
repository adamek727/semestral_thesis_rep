#ifndef KINECTDEVICEREADER_H
#define KINECTDEVICEREADER_H


#include "OpenNI.h"
#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
#include <array>


// OpenCV Header
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/calib3d/calib3d.hpp>


using namespace openni;
using namespace cv;
using namespace std;

class KinectDeviceReader
{
public:
    KinectDeviceReader();

    ~KinectDeviceReader();

    Mat getColorFrame();

    Mat getDepthFrame();

private:

    Device device;

    VideoStream DepthStream,ColorStream;

    VideoFrameRef DepthFrameRead,ColorFrameRead;
};

#endif // KINECTDEVICEREADER_H
