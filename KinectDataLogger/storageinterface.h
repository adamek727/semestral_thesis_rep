#ifndef STORAGEINTERFACE_H
#define STORAGEINTERFACE_H

#include <string>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include<dirent.h>

#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/opencv.hpp>
#include <opencv2/imgcodecs.hpp>


using namespace std;
using namespace cv;

class StorageInterface
{
public:
    StorageInterface();

    void makeFolderWithName(string foldeName);

    void saveImageWithPath(Mat image, string imagePath);

private:

    bool folderCreated = false;

    bool doesDirectoryExist(string path);
};



#endif // STORAGEINTERFACE_H
