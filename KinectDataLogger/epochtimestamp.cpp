#include "epochtimestamp.h"



EpochTimestamp::EpochTimestamp()
{

}



void EpochTimestamp::setupCurrentTime(){


    struct timeval tp;
    gettimeofday(&tp, NULL);
    nowTime = tp.tv_sec;
    milisecTime = tp.tv_sec * 1000 + tp.tv_usec / 1000;
}


string EpochTimestamp::getTimestampString(){


    struct tm  tstruct;
    char       buf[80];
    tstruct = *localtime(&nowTime);

    // Visit http://en.cppreference.com/w/cpp/chrono/c/strftime
    // for more information about date/time format
    strftime(buf, sizeof(buf), "%Y-%m-%d.%X", &tstruct);
    string str(buf);
    return str;
}


string EpochTimestamp::getMilisecondTimestampString(){

    struct tm tstruct;
    char buf[80];
    char time[255];

    struct timeval tp;
    gettimeofday(&tp, NULL);
    tstruct = *localtime(&tp.tv_sec);
    milisecTime = tp.tv_sec * 1000 + tp.tv_usec / 1000;

    strftime(buf, sizeof(buf), "%X", &tstruct);
    sprintf(time, "%s:%03ld", buf, milisecTime%1000);
    string str(time);

    return str;
}
