#ifndef EPOCHTIMESTAMP_H
#define EPOCHTIMESTAMP_H


#include <iostream>
#include <string>
#include <stdio.h>
#include <time.h>
#include <chrono>
#include <sys/time.h>

using namespace std;

class EpochTimestamp
{
public:

    EpochTimestamp();

    void setupCurrentTime();

    string getTimestampString();

    string getMilisecondTimestampString();

private:

    time_t nowTime;
    time_t milisecTime;
};

#endif // EPOCHTIMESTAMP_H
