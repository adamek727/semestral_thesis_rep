#include "kinectdevicereader.h"

KinectDeviceReader::KinectDeviceReader(){

    const char* deviceURI = openni::ANY_DEVICE;

    Status result = STATUS_OK;
    result = OpenNI::initialize();
    result = device.open(deviceURI);
    result = DepthStream.create(device, openni::SENSOR_DEPTH);
    result = DepthStream.start();
    result = ColorStream.create(device, openni::SENSOR_COLOR);
    result = ColorStream.start();

    //device.setImageRegistrationMode(ImageRegistrationMode::IMAGE_REGISTRATION_DEPTH_TO_COLOR);
}



KinectDeviceReader::~KinectDeviceReader(){

    DepthStream.destroy();
    ColorStream.destroy();
    device.close();
    OpenNI::shutdown();
}



Mat KinectDeviceReader::getColorFrame(){

    if (ColorStream.readFrame(&ColorFrameRead) == STATUS_OK)
            {
        Mat BGRChannels[3], RGBChannels[3], frame, outputFrame;

        ColorStream.readFrame(&ColorFrameRead);
        const openni::RGB888Pixel* imageBuffer = (const openni::RGB888Pixel*)ColorFrameRead.getData();

        frame.create(ColorFrameRead.getHeight(), ColorFrameRead.getWidth(), CV_8UC3);
        memcpy(frame.data, imageBuffer, 3 * ColorFrameRead.getHeight()*ColorFrameRead.getWidth() * sizeof(uint8_t));

        split(frame,BGRChannels);

        RGBChannels[0] = Mat(BGRChannels[2]);
        RGBChannels[1] = Mat(BGRChannels[1]);
        RGBChannels[2] = Mat(BGRChannels[0]);

        merge(RGBChannels,3,outputFrame);
        return outputFrame;
    }else{
        cout << "unable to read color frame from RGB-D camera" << endl;
    }
    return Mat();
}



Mat KinectDeviceReader::getDepthFrame(){

    if (DepthStream.readFrame(&DepthFrameRead) == STATUS_OK){
        cv::Mat cDepthImg(DepthFrameRead.getHeight(), DepthFrameRead.getWidth(),CV_16UC1, (void*)DepthFrameRead.getData());
        cv::Mat c8BitDepth;
        cDepthImg.convertTo(c8BitDepth, CV_8U, 255.0 / (8000));
        return cDepthImg;
    }else{
        cout << "unable to read depth frame from RGB-D camera" << endl;
    }
    return Mat();
}
