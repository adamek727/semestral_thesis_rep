#ifndef GUIPREZENTER_H
#define GUIPREZENTER_H


#include <string>

#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>


using namespace std;
using namespace cv;

class GuiPrezenter
{
public:
    GuiPrezenter();

    void drawImageWithName(Mat image, string name);
};

#endif // GUIPREZENTER_H
