#include <QCoreApplication>

#include "kinectdatalogger.h"

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    KinectDataLogger dataLoggerApp;
    dataLoggerApp.mainLoop();

    return a.exec();
}
