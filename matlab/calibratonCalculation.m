clear all;
close all;
clc;

data = load('rgbCalibrationSession.mat');
intrinsicMatrix = data.calibrationSession.CameraParameters.IntrinsicMatrix;
radial = data.calibrationSession.CameraParameters.RadialDistortion;
tangential = data.calibrationSession.CameraParameters.TangentialDistortion;
resolution = [1080 1920];

pixelSize = 3.1e-6;

principalPoint = ((resolution /2) - intrinsicMatrix(3,1:2))  * pixelSize
focalLength =  sum([intrinsicMatrix(1, 1) intrinsicMatrix(2, 2)] * pixelSize) / 2

focalLenghtInPixels = focalLength/pixelSize;
principalPointInPixels = principalPoint/pixelSize;


[X,Y] = meshgrid(-960:40:960,-540:40:540);
X = (X+principalPointInPixels(1)) * pixelSize;
Y = (Y+principalPointInPixels(2)) * pixelSize;


Zx = zeros([size(Y,1) size(X,2)]);
for i = 1 : size(Y,1)
    for j = 1 : size(X,2)
        distance = sqrt( X(i,j)^2 + Y(i,j)^2 );
        distancePow = distance^2;
        
        Zx(i,j) =  -X(i,j)* (1 + radial(1)*distancePow + radial(2)*distance^4);
        Zy(i,j) =  -Y(i,j)* (1 + radial(1)*distancePow + radial(2)*distance^4);
        
        Tx(i,j) =  -X(i,j) * ( 2 * tangential(1) * X(i,j) * Y(i,j) + tangential(2) * (distancePow + 2 * X(i,j)^2));
        Ty(i,j) =  -Y(i,j) * ( tangential(1) * (distancePow + 2 * Y(i,j)^2) + 2 * tangential(2) * X(i,j) * Y(i,j));
        
    end
end





figure
hold on
title('RGB Radial lense distortion')
xlabel('Frame width [mm]')
ylabel('Frame height [mm]')
quiver(X,Y,Zx,Zy)
plot(0,0,'rx')
plot(principalPoint(2),principalPoint(1),'bo')
Z = sqrt( Zx.^2 + Zy.^2);
[C,h] = contour(X,Y,Z); 
clabel(C,h);

figure
hold on
title('RGB Tangential lense distortion')
xlabel('Frame width [mm]')
ylabel('Frame height [mm]')
quiver(X,Y,Tx,Ty)
plot(0,0,'rx')
plot(principalPoint(2),principalPoint(1),'bo')
Z = sqrt( Tx.^2 + Ty.^2);
[C,h] = contour(X,Y,Z); 


figure
hold on
title('RGB Total lense distortion')
xlabel('Frame width [mm]')
ylabel('Frame height [mm]')
Kx = Zx+Tx;
Ky = Zy+Ty;
quiver(X,Y,Kx,Ky)
plot(0,0,'rx')
plot(principalPoint(2),principalPoint(1),'bo')
K = sqrt( Kx.^2 + Ky.^2);
[C,h] = contour(X,Y,K); 

clabel(C,h);