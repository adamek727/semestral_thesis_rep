% clear all;
close all;
clc;

h = 480;
w = 640;
no = 100;

irH = 424;
irW = 511;

avgImage = double(zeros(irH,irW));
stdDivImage = double(zeros(irH,irW));
% images = zeros(irH,irW,no);
% for i = 1 : no
%     i
%     path = sprintf('images/%d.m', i);
%     data = load(path);
%     images(:,:,i) = data(1:irH, 1:irW); 
% end



for i = 1 : irH
    for j = 1 : irW
        
        timeLinePixel = images(i,j,1:no);
        ind = find(timeLinePixel == 0);
        if(~isempty(ind))
            avgImage(i,j) = 0;
            stdDivImage(i,j) = 0;
        else
            avgImage(i,j) = sum(timeLinePixel)/no;
            stdDivImage(i,j) = std(timeLinePixel,0,3);
        end
    end
end

figure
imshow(avgImage(:,:),'Colormap',jet(uint32(max(max(avgImage)))))
figure
imshow(stdDivImage(:,:),'Colormap',jet(max(max(stdDivImage))))

avgStdDiv = sum(sum(stdDivImage(find(stdDivImage~=0))))/length(find(stdDivImage~=0))